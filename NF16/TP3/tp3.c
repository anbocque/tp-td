#include "tp3.h"


// alloue la mémoire et crée une note
 T_Note* creerNote(float note, char *matiere){
    if(note>=0 && note<=20){
        T_Note* nouveau = (T_Note*)malloc(sizeof(T_Note));
        nouveau->note = note;
        nouveau->matiere = matiere;
        nouveau->suivant = NULL;
        return nouveau;
    }
}

/* -------------------------------------------------------------------*/
// alloue la mémoire pour une liste vide
T_ListeEtu creerListe(){
    T_ListeEtu l = (T_ListeEtu)(malloc(sizeof(T_Etudiant)));
    l->identifiant = -1; 
}

/* -------------------------------------------------------------------*/

// alloue la mémoire et crée un étudiant
T_Etudiant *creerEtudiant(int idEtu, char *nom, char *prenom){
    T_Etudiant* e = (T_Etudiant*)(malloc(sizeof(T_Etudiant)));
    e->identifiant = idEtu;
    e->nom = nom;
    e->prenom = prenom;
    e->nbrNotes = 0;
    e->moyenne = -1;
    e->liste = NULL;
    e->suivant = NULL;

}

/* -------------------------------------------------------------------*/

// ajoute une note à une liste de notes passée en paramètre
T_ListeNotes ajouterNote(float note, char *matiere, T_ListeNotes listeNotes){
     T_Note* nouveau = creerNote(note, matiere);
     if(listeNotes)
        nouveau->suivant = listeNotes;
     else{
        nouveau = (T_ListeNotes)(malloc(sizeof(T_Note)));
        nouveau->note = note;
        nouveau->matiere = matiere;
        nouveau->suivant = NULL;
     }
     return nouveau;
 }

 /* -------------------------------------------------------------------*/

// ajoute un étudiant dans une liste en respectant l'ordre des id croissants

T_ListeEtu ajouterEtuListe(T_Etudiant* etu, T_ListeEtu listeEtu){
    T_ListeEtu temp = listeEtu;

    if(listeEtu->identifiant == -1){ //Si la liste est vide on retourne l'étudiant
        listeEtu = (T_ListeEtu)etu;
        return listeEtu;
    }
    else{        
        T_ListeEtu prec = NULL;

        // on parcoure la liste jusqu'à dépasser l'id ou arriver en fin de liste
        while(temp->suivant && temp->identifiant < etu->identifiant){
            prec = temp;
            temp = temp->suivant;
        }


        if(temp->identifiant == etu->identifiant){// cas où l'étudiant existe déjà.

            printf("\nL'étudiant existe déjà.\n");

        // on insère l'étudiant au bon endroit selon son id et sa position dans la chaîne.
        }else if(temp->identifiant < etu->identifiant){
            etu->suivant = NULL;
            temp->suivant = etu;
            return listeEtu;
        }
        else{
            if(prec){
                prec->suivant = etu;
                etu->suivant = temp;
                return listeEtu;
            }
            else {
                if(temp->identifiant > etu->identifiant){
                    etu->suivant = temp;
                    return (T_ListeEtu)etu;
                }else{
                    temp->suivant = etu;
                    return temp;
                }
            }
        }
    }
}

 /* -------------------------------------------------------------------*/

// ajout d'une note à un étudiant
T_ListeEtu ajouterNoteEtu(float note, char *matiere, int idEtu, T_ListeEtu listeEtu){
    T_ListeEtu prec;
    T_ListeEtu temp = listeEtu;

    char* mat = malloc(sizeof(char)*20);
    strcpy(mat, matiere);

    // on parcoure la chaîne.
    while(temp && temp->identifiant != idEtu){
        prec = temp;
        temp = temp->suivant;
    }

    //si l'étudiant existe on ajoute la note.
    if(temp && temp->identifiant == idEtu){
        temp->liste = ajouterNote(note,mat,temp->liste);
        temp->nbrNotes++;
        return listeEtu;
    }
    else{
        //sinon on crée l'étudiant, on lui ajoute la note et on l'insère dans la liste en respectant l'ordre des id
        char* nom = malloc(sizeof(char)*20);
        char* prenom = malloc(sizeof(char)*20);

        printf("L'étudiant avec l'id %d n'existe pas, entrez son nom : ", idEtu);
        fflush(stdin);
        scanf("%s", nom);

        printf("Entrez son prenom : ");
        fflush(stdin);
        scanf("%s", prenom);

        T_ListeEtu e = creerEtudiant(idEtu, nom, prenom);
        e->liste = ajouterNote(note,mat,e->liste);
        e->suivant=NULL;
        e->nbrNotes=1;
        listeEtu = ajouterEtuListe(e, listeEtu);

        return listeEtu;
    }
}

 /* -------------------------------------------------------------------*/


// suppression d'une note d'un étudiant
T_ListeEtu supprimerNoteEtu(char *matiere, int idEtu, T_ListeEtu listeEtu){

    T_ListeEtu current = listeEtu;
    T_ListeEtu previous = NULL;

    // on parcoure la chaine
    while(current->suivant && current->identifiant < idEtu){
        previous = current;
        current = current->suivant;
    }

    // cas où l'étudiant n'existe pas
    if(current->identifiant != idEtu){
        printf("L'étudiant n'existe pas.\n");
        return listeEtu;
    }

    T_ListeNotes current_note = current->liste;
    T_ListeNotes previous_note = NULL;

    // on cherche la note qui correspond à la matière
    while(strcmp(current_note->matiere, matiere) != 0 && current_note != NULL){
        previous_note = current_note;
        current_note = current_note->suivant;
    }

    // cas où la note n'existe pas
    if(current_note == NULL){
        printf("La note n'existe pas.\n");
        return listeEtu;
    }

    // on supprime la note et on libère la mémoire
    if(previous_note == NULL){
        T_ListeNotes temp = current->liste;
        current->liste = current->liste->suivant;
        free(temp);
    }
    else{
        previous_note->suivant = current_note->suivant;
        free(current_note);
        }

    current->nbrNotes--;

    // si l'étudiant n'a plus de note on le supprime et on libère la mémoire
    if(current->nbrNotes == 0){
        printf("\nSuppression de l'étudiant avec l'id %d car il n'a plus de notes.\n\n", current->identifiant);
        if(previous == NULL){
            T_ListeEtu temp = current;
            listeEtu = current->suivant;
            free(temp);
        } else{
            previous->suivant = current->suivant;
            free(current);
        }
    }

    return listeEtu;
}

 /* -------------------------------------------------------------------*/

// affichage d'une liste d'étudiants avec leurs notes
void afficherListeEtu(T_ListeEtu listeEtu){
    T_ListeEtu temp = listeEtu;

    if(temp == NULL) printf("\nAucun étudiant dans la liste.");

    while(temp){
        printf("Nom: %s, prenom: %s, liste des notes: \n",temp->nom, temp->prenom);

        T_ListeNotes l = temp->liste;
        while(l){
            printf("Matiere: %s \t Note obtenue: %d \n",l->matiere, l->note);
            l = l->suivant;
        }

        if(temp->moyenne>0) printf("Pour une moyenne de %.2f \n",temp->moyenne);
        temp = temp->suivant;
    }
}

 /* -------------------------------------------------------------------*/

// calcul de la moyenne d'un étudiant
float calculMoyenneEtu(T_Etudiant* etu){
    int i;
    float somme=0;
    T_ListeNotes noteActuelle = etu->liste;
    for(i = 0; i < etu->nbrNotes; i++){
        somme += noteActuelle->note;
        noteActuelle = noteActuelle->suivant;
    }
    return somme/etu->nbrNotes;
}

/* -------------------------------------------------------------------*/

// calcul de la moyenne d'une liste d'étudiants
void calculMoyenne(T_ListeEtu liste){
    while(liste){
        liste->moyenne = calculMoyenneEtu(liste);
        liste=liste->suivant;
    }
}


/* -------------------------------------------------------------------*/

// affichage du classement des étudiants en fonction de leur moyenne
void afficherClassement(T_ListeEtu listeEtu){
    printf("\n------------ Affichage du classement -------------\n");
    T_ListeEtu p_aux = listeEtu;

    // calcul de la moyenne de chaque étudiant
    calculMoyenne(listeEtu);
  
    // cas où la liste est vide
    if(p_aux == NULL){
        printf("-- Fin de la liste --\n\n");       
    }
    else {
    
        int nbEtu = 0;

        // on compte le nombre d'étudiant présent dans notre liste pour allouer la mémoire en fonction
        while (p_aux != NULL) {
            p_aux = p_aux->suivant;
            nbEtu++;
        }
    
         // on alloue un tableau d'étudiants
        T_ListeEtu *tabEtu = malloc(nbEtu*sizeof(T_ListeEtu));
        p_aux = listeEtu;
        
        int i,j;
        
        // on remplit le tableau
        for(i=0; i<nbEtu; i++){
            tabEtu[i] = p_aux;
            p_aux = p_aux->suivant;
        }
        
        // on trie le tableau
        for (i=0; i<nbEtu; i++) {
            for (j=i; j<nbEtu; j++) {
                if(tabEtu[j]->moyenne < tabEtu[i]->moyenne){
                    p_aux= tabEtu[i];
                    tabEtu[i] = tabEtu[j];
                    tabEtu[j] = p_aux;
                }
            }
        }
        // affichage
        printf("\nClassement des étudiants : \n");
        for(i = nbEtu -1 ; i >= 0; i--){
            printf("%.2f %s %s\n",  tabEtu[i]->moyenne, tabEtu[i]->nom, tabEtu[i]->prenom);
        }
        printf("-- Fin de la liste --\n");
       
    }
   
}

/* -------------------------------------------------------------------*/

// ajouter un étudiant en tête d'une liste peu importe l'ordre des id
T_ListeEtu ajouterEtu(int idEtu, char *nom, char *prenom, T_ListeEtu listeEtu){
    T_Etudiant* e = creerEtudiant(idEtu, nom, prenom);

    if(listeEtu->identifiant != -1) e->suivant = listeEtu;
    return e;
 }

/* -------------------------------------------------------------------*/

// création de sous listes pour une matière
void sousListes(T_ListeEtu listeEtu, char* matiere){

    T_ListeEtu current = listeEtu;

    // initialisation des listes
    T_ListeEtu listeSucces = creerListe();
    T_ListeEtu listeEchecs = creerListe();
    T_ListeEtu listeNonInscrits = creerListe();

    // on parcoure notre liste d'étudiants
	while(current != NULL){

		T_ListeNotes currentNote = current->liste;

 
		while(currentNote != NULL && strcmp(currentNote->matiere, matiere) != 0){
			currentNote = currentNote->suivant;
		}

        // on ajoute l'étudiant dans la bonne liste
		if(currentNote == NULL) listeNonInscrits = ajouterEtu(current->identifiant, current->nom, current->prenom, listeNonInscrits);
		else if (currentNote->note < 10.0) listeEchecs = ajouterEtu(current->identifiant, current->nom, current->prenom, listeEchecs);
		else if (currentNote->note >= 10.0) listeSucces = ajouterEtu(current->identifiant, current->nom, current->prenom, listeSucces);

		current = current->suivant;
	}

    // affichage
    if(listeNonInscrits->identifiant == -1){
        printf("\nTous les étudiant sont inscris.\n");
    } else{
        printf("\nNon inscrits : \n");
        afficherSousListe(listeNonInscrits);
    }

    if(listeEchecs->identifiant == -1){
        printf("\nAucun échec.\n");
    } else{
        printf("\nListe échecs : \n");
        afficherSousListe(listeEchecs);
    }

    if(listeSucces->identifiant == -1){
        printf("\nAucune réussite.\n");
    }else{
        printf("\nListe succès :\n");
        afficherSousListe(listeSucces);
    }
    	
}
/* -------------------------------------------------------------------*/

//fonction d'affichage pour sousListes
void afficherSousListe(T_ListeEtu listeEtu){
    if(listeEtu == NULL) printf("\nAucun étudiant dans la liste.");
    while(listeEtu){
        printf("Nom: %s, prenom: %s\n",listeEtu->nom, listeEtu->prenom);
        listeEtu = listeEtu->suivant;
    }
}

/* -------------------------------------------------------------------*/

// fonctions de libération de la mémoire

void libererEtu(T_Etudiant * etu){
    T_ListeNotes current = etu->liste;
    T_ListeNotes temp;

    while(current){
        temp = current;
        current = current->suivant;
        free(temp);
    }

    free(etu);
}

void libererListeEtu(T_ListeEtu liste){
    T_ListeEtu current = liste;
    T_ListeEtu temp;

    while(current){
        temp = current;
        current = current->suivant;
        libererEtu(temp);
        free(temp);
    }
}