#include <stdio.h>

int main(int argc, char const *argv[])
{
  int i;
  int *j;

  i = 5;
  j = &i;

  *j = *j + 1;

  printf("i = %d\n", *j);

  i *= 5;

  printf("i = %d\n", *j);

  j++;

  printf("*j = %d\n", *j);


  return 0;
}
