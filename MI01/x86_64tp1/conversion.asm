; conversion.asm
;
; MI01 - TP Assembleur 1
;
; Affiche un nombre de 64 bits sous forme lisible

title conversion.asm

extern      putchar:near
extern      getchar:near

.data

nombre      dq      141f069d082h    ; Nombre � convertir
chaine      db      xx dup(?)       ; Remplacer xx par la longueur maximale n de la cha�ne

.code

; Sous-programme main, automatiquement appel� par le code de
; d�marrage 'C'
public      main
main        proc
            
            push    rbx
            push    rsi
            push    rdi
            sub     rsp, 32         ; Allocation 'shadow space'

            ; ************************
            ; Ajoutez votre code ici !
            ; ************************

            call    getchar         ; Attente de l'appui sur "Entr�e"

            add     rsp, 32         ; Lib�ration 'shadow space'
            push    rbx
            push    rsi
            push    rdi
            ret                     ; Retour au code de d�marrage 'C'

main        endp

            end