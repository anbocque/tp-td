#include <stdio.h>

int main(int argc, char const *argv[])
{
  int a, b, temp = 0;
  printf("a = ?");
  scanf("%d", &a);
  printf("\nb = ?");
  scanf("%d", &b);

  // inversion
  temp = a;
  a = b;
  b = temp;

  printf("\na = %d , b = %d\n", a, b);

  return 0;
}
