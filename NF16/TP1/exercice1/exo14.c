#include <stdio.h>

int main(int argc, char const *argv[])
{
  float f, c;

  printf("Entrez une température en Fahrenheit : ");
  scanf("%f", &f);

  c = (5/9)*(f - 32);

  printf("\n %f Fahrenheit = %f Celsius\n", f, (f-32)*5/9);

  return 0;
}
