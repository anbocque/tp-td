#include <stdio.h>

int main(int argc, char const *argv[])
{
  int a = 0;

  printf("Entrez un entier : ");
  scanf("%d", &a);

  printf("\n%d en hexadecimal vaut : %x\n", a, a);
  printf("\n%d en octal vaut : %o\n", a, a);

  return 0;
}
