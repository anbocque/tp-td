#include "mystrncpy.h"

void mystrncpy(char *s1, char *s2, int n) {
  int l1 = strlen(s1);
  if (n > l1){
    printf("ERREUR : n est plus grand que la longeur de l1\n");
  }
  else{
    int i = -1, ecart = l1 -n;
    while (s1[++i + ecart] != 0){
        s2[i] = s1[i + ecart];
      }
    s2[i] = 0;
  }
  return;
};