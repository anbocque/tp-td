#include <stdio.h>
#include <stdlib.h>
#include "tp3.h"

int main(){

    
    int continuer = 1;
    int choix;
    T_ListeEtu liste = creerListe();

    while(continuer){
        printf("\n##############################################################\n");
        printf("Choisissez une option : \n");
        printf("1. Créer la liste d'étudiants\n");
        printf("2. Ajouter une note pour un étudiant\n");
        printf("3. Supprimer une note pour un étudiant\n");
        printf("4. Afficher la liste des étudiants\n");
        printf("5. Afficher le classement\n");
        printf("6. Afficher les sous-listes pour une épreuve\n");
        printf("7. Quitter\n");

        scanf("%d", &choix);
        fflush(stdin);

        printf("\n##############################################################\n");

        if(choix == 1){
            int i = 1;
             
           while(i){
               int id;
               char* nom = malloc(sizeof(char)*20);
               char* prenom = malloc(sizeof(char)*20);
               printf("Entrez l'id de l'étudiant : ");
               scanf("%d", &id);
               fflush(stdin);
               printf("Entrez le nom de l'étudiant %d : ", id);
               scanf("%s", nom);
               fflush(stdin);
               printf("Entrez le prenom de l'étudiant %d : ", id);
               scanf("%s", prenom);
               fflush(stdin);
               T_Etudiant* etu = creerEtudiant(id, nom, prenom);
               liste = ajouterEtuListe(etu, liste);
               id++;
               printf("Ajouter un autre étudiant ? 1 = oui, 0 = non : ");
               scanf("%d", &i);
               fflush(stdin);
           }
        }else if(choix == 2){
            int id_etu;
            float note;
            char matiere[20];
            printf("Entrez l'id de l'étudiant : ");
            scanf("%d", &id_etu);
            fflush(stdin);
            printf("Entrez le nom de la matière : ");
            scanf("%s", matiere);
            fflush(stdin);
            printf("Entrez la note de l'étudiant : ");
            scanf("%f", &note);
            fflush(stdin);

            liste = ajouterNoteEtu(note, matiere, id_etu, liste);

        }else if(choix == 3){
            int id_sup;
            char uv[20];
            printf("Entrez l'id de l'étudiant : ");
            scanf("%d", &id_sup);
            fflush(stdin);
            printf("Entrez le nom de la matière : ");
            scanf("%s", uv);
            fflush(stdin);

            liste = supprimerNoteEtu(uv, id_sup, liste);

        }else if(choix == 4){
            afficherListeEtu(liste);
        }else if(choix == 5){
            afficherClassement(liste);
        }else if(choix == 6){
            char mat[20];
            printf("Choisissez une matière : ");
            scanf("%s", mat);
            fflush(stdin);
            sousListes(liste, mat);
        }
        else{
            continuer = 0;
            libererListeEtu(liste);
        }
    }

    return 0;
}
