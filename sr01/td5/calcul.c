#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
  if(strcmp(argv[1], "-a") == 0){
    int sum = 0;
    for(int i = 2; i < argc; i++){
      sum += atoi(argv[i]);
    }
    printf("%d\n", sum);
  } else if(strcmp(argv[1], "-p") == 0){
    int prod = 1;
    for(int i = 2; i < argc; i++){
      prod *= atoi(argv[i]);
    }
    printf("%d\n", prod);
  } else if(strcmp(argv[1], "-d") == 0){
    double res = 0;
    res = atof(argv[2]) / atof(argv[3]);
    printf("%f\n", res);
  } else{
    printf("Commande non reconnue.\n");
  }
}
