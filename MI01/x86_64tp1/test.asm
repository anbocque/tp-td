  extern putchar

  section .data
  msg db "bonjour tout le monde", 0

  section .text
            push  rbx             ; Sauvegarde pour le code 'C'
            sub rsp, 32         ; Allocation shadow space

            mov rbx, 0
            ; On suppose que la longueur de la cha�ne est non nulle
            ; => pas de test de la condition d'arr�t au d�part
suivant:    movzx   ecx, byte ptr[rbx + msg]
            mov     esi, ecx

            call    putchar         ; Appel de putchar, caract�re dans ecx
            
            inc     rbx             ; Caract�re suivant
            cmp     esi, 0 ; Toute la longueur ?
            jne     suivant         ; si non, passer au suivant

            call    getchar         ; Attente de l'appui sur "Entr�e"

            add     rsp, 32         ; Lib�ration shadow space
            pop     rbx             ; Restituttion de rbx sauvegard�

            ret                     ; Retour au code de d�marrage 'C'

main        endp

            end