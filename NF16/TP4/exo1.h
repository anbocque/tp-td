#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct noeud{
  int cle;
  struct noeud* suivant;
} t_noeud;

typedef t_noeud* t_liste;

void afficher_liste(t_liste liste);
t_liste generer_liste_aleatoire(int n);
void afficher_liste_inverse(t_liste liste);