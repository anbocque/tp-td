#include <stdio.h>

int main(int argc, char const *argv[])
{
  char a;

  printf("Entrez un chiffre ou un caractère : ");
  scanf("%c", &a);

  if(a > 47 && a < 58){
    printf("Le caractère entré est un chiffre\n");
  }

  if(a > 64 && a < 91){
    printf("Le caractère entré est une lettre majuscule\n");
  }

  if(a > 96 && a < 123){
    printf("Le caractère entré est une lettre minuscule\n");
  }


  return 0;
}
