#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, char const *argv[])
{
  pid_t pid1;
  pid_t pid2;


  if(!(pid1 = fork())){
    printf("Je suis le processus fils avec le pid : %d\nLe pid de mon père est : %d\n", getpid(), getppid());
  }else{
    if(!(pid2 = fork())){
      printf("Je suis le processus fils avec le pid : %d\nLe pid de mon père est : %d\n", getpid(), getppid());
    }else{
      printf("Je suis le processus père avec le pid : %d\n", getpid());
    }
  }

  

  return 0;
}
