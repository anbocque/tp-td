#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, char const *argv[])
{
  printf("\nParent id : %d", getppid());
  printf("\nEnfant id : %d\n", getpid());
  
  return 0;
}
