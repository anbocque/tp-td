#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Note
{
    int note;
    char* matiere;
    struct Note* suivant;
};

typedef struct Note T_Note;
typedef T_Note* T_ListeNotes;

struct Etudiant
{
    int identifiant;
    char* nom;
    char* prenom;
    T_ListeNotes liste;
    int nbrNotes;
    float moyenne;
    struct Etudiant* suivant;
};

typedef struct Etudiant T_Etudiant;
typedef T_Etudiant * T_ListeEtu;

T_Note* creerNote(float note, char *matiere);
T_Etudiant *creerEtudiant(int idEtu, char *nom, char *prenom);

T_ListeNotes ajouterNote(float note, char *matiere, T_ListeNotes listeNotes);
T_ListeEtu ajouterNoteEtu(float note, char *matiere, int idEtu, T_ListeEtu listeEtu);
T_ListeEtu ajouterEtuListe(T_Etudiant* etu, T_ListeEtu listeEtu);


T_ListeEtu supprimerNoteEtu(char *matiere, int idEtu, T_ListeEtu listeEtu);

void afficherListeEtu(T_ListeEtu listeEtu);


T_ListeEtu ajouterEtu(int idEtu, char *nom, char *prenom, T_ListeEtu listeEtu);
T_ListeEtu creerListe();

float calculMoyenneEtu(T_Etudiant* etu);
void calculMoyenne(T_ListeEtu);
void afficherClassement(T_ListeEtu liste);

void sousListes(T_ListeEtu listeEtu, char* matiere);
void afficherSousListe(T_ListeEtu);

void libererEtu(T_Etudiant * etu);
void libererListeEtu(T_ListeEtu liste);