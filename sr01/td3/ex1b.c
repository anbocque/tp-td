#include <stdio.h>

#define MAX 5


int main(int argc, char const *argv[])
{
  int tab1[MAX] = {6,4,5,10,8};
  int tab2[MAX] = {7,8,9,10,11};
  int tab3[MAX];

  int* pt1 = tab1;
  int* pt2 = tab2;
  int* pt3 = tab3;

  for(int i = 0; i<5; i++){
    pt3[i] = pt1[i] + pt2[(MAX-1) - i];
    printf("%d\n", pt3[i]);
  }


  return 0;
}
