#include <unistd.h>
#include <stdio.h>

int main(int argc, char const *argv[])
{
  extern char** __environ;
  int i = 0;
  do{
    printf("%s\n", __environ[i++]);
  }while(__environ[i] != NULL);
  return 0;
}
