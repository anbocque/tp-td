#include <stdio.h>


int fibo1(int n);
int fibo2(int n);


int main(int argc, char const *argv[])
{
  /*
  for(int i = 0; i < 10; i++){
    printf("%d\n", fibo1(i));
  }
  */

  for(int i = 0; i < 10; i++){
    printf("%d\n", fibo2(i));
  }

  return 0;
}


/* ---------------------- */

int fibo1(int n){

  if(n == 0){
    return 0;
  }
  if(n == 1){
    return 1;
  }

  return fibo1(n -1) + fibo1(n - 2);

}

/* ---------------------- */

int fibo2(int n){
  if(n == 0){ 
    return 0;
  }
  else if(n == 1){
    return 1;
  }
  else if(n%2 == 0){
    return fibo2(n/2)*fibo2(n/2) + 2*fibo2(n/2)*fibo2((n/2) - 1);
  } 
  else{
    return fibo2((n-1)/2)*fibo2((n-1)/2) + fibo2((n-1)/2 + 1)* fibo2((n-1)/2 + 1);
  }
}