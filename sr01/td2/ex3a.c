#include <stdio.h>
#include "ex3.h"

void afficher(complexe);

int main(int argc, char const *argv[])
{
  complexe a;

  a.x = 3;
  a.y = 4;

  afficher(a);

  return 0;
}