#include <stdio.h>

void permute(int*, int*);


int main(int argc, char const *argv[])
{
  int a, b;

  printf("Entrez a : ");
  scanf("%d", &a);

  printf("Entrez b : ");
  scanf("%d", &b);

  permute(&a, &b);

  printf("a = %d et b = %d \n", a, b);

  return 0;
}

// On utilise des pointeurs pour faire un passage par adresse et ainsi avoir accès aux variables et non à une copie

void permute(int *a, int *b){
  int temp = *a;
  *a = *b;
  *b = temp;
}