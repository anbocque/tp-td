#include <stdio.h>

int main(int argc, char const *argv[])
{
  int i = 0;

  printf("Entrez un entier : ");
  scanf("%d", &i);

  if(i == 0){
    printf("\nLe nombre entré vaut 0.\n");
  } else{
    if(i % 2 == 0){
      printf("\nLe nombre entré est pair.\n");
    } else{
      printf("\nLe nombre entré est impair.\n");
    }
  }


  return 0;
}
