#include <stdio.h>
#include "ex3.h"

void afficher(complexe c){
  if((int)c.y < 0){
    printf("%f - %fi\n", c.x, c.y);
  }
  if((int)c.y == 0) {
    printf("%f\n", c.x);
  }
  if((int)c.y > 0) {
    printf("%f + %fi\n", c.x, c.y);
  }
}

complexe somme(complexe a, complexe b){
  complexe ret;
  ret.x = a.x + b.x;
  ret.y = a.y + b.y;

  return ret;
}