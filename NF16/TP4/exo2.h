#include <stdio.h>
#include <stdlib.h>

#define MAX_N 4

typedef struct Noeud{
  int tab[MAX_N];
  int nb;
  struct Noeud* suivant;
} T_Noeud;

typedef T_Noeud* T_List;

T_List creerNoeud();
void afficherListe(T_List maListe);