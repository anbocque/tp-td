#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, char const *argv[])
{
  pid_t pid;

  if(pid = fork()){
    printf("Je suis le processus père avec le pid : %d, fils = %d\n", getpid(), pid);
  }else{
    printf("\nJe suis le processus fils avec le pid : %d\nLe pid de mon père est : %d\n", getpid(), getppid());
  }

  return 0;
}
