#include <stdio.h>
#include <stdlib.h>

#define MAX_F 10

typedef struct file{
  int tete;
  int queue;
  int tab[MAX_F];
} file_t;

file_t* creer_file();
int file_vide(file_t* file);
void enfiler(file_t* file, int val);

int main(int argc, char const *argv[])
{
  file_t* file = creer_file();
  
  printf("%d, %d\n", file->queue, file->tete);

  enfiler(file, 7);

  printf("%d, %d\n", file->queue, file->tete);
  printf("%d, %d\n", file->tab[file->queue], file->tab[file->tete]);

  return 0;
}


file_t* creer_file(){
  file_t* file = malloc(sizeof(file_t));
  file->tete = 0;
  file->queue = 0;
};

int file_vide(file_t* file){
  if(file->tete == file->queue) return 1;
  return 0;
};

void enfiler(file_t* file, int val){
  file->tab[file->queue] = val;
  if(file->queue == MAX_F - 1){
    file->queue = 0;
  }else{
    file->queue++;
  }
}