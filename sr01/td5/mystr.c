#include "mystr.h"

void mystrupdown(char *s1, char *s2){
  for(int i = 0; i < strlen(s1); i++){
    if((s1[i] >= 'A') && (s1[i] <= 'Z')) {
      s2[i] = s1[i] - 'A' + 'a';
    } else if((s1[i] >= 'a') && (s1[i] <= 'z')){
      s2[i] = s1[i] - 'a' + 'A';
    } else{
      s2[i] = s1[i];
    }
  }
}

void mystrinv(char *s1, char *s2) {
  int l1 = strlen(s1);
  int i = -1;
  while (s1[++i] != 0){
      s2[i] = s1[l1-1-i];
    }
  s2[i] = 0;
  return;
}

int mystrchrn(char *s, char c) {
  int res = 0, i = -1;
  while(s[++i] != 0)
    if(s[i] == c)
      res++;
  return res;
}

void mystrncpy(char *s1, char *s2, int n) {
  int l1 = strlen(s1);
  if (n > l1){
    printf("ERREUR : n est plus grand que la longeur de l1\n");
  }
  else{
    int i = -1, ecart = l1 -n;
    while (s1[++i + ecart] != 0){
        s2[i] = s1[i + ecart];
      }
    s2[i] = 0;
  }
  return;
}

void mystrncat(char *s1, char *s2, int n) {
  int l1 = strlen(s1), l2 = strlen(s2);
  int i = -1,j;
  while (s2[++i] != 0){
  }
  printf("i = %d\n",i);
  for(j=0; j < n;j++){
    s2[i+j] = s1[l1-n+j];
  }
  s2[i+j] = 0;
  return;
}
