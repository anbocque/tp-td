#include <stdio.h>

int main(int argc, char const *argv[])
{
  int i;
  int *j;

  i = 5;
  j = &i;

  printf("i = %d\nj = %p\n*j = %d\n", i, j, *j);


  return 0;
}
