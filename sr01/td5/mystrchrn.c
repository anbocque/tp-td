#include "mystrchrn.h"

int mystrchrn(char *s, char c) {
  int res = 0, i = -1;
  while(s[++i] != 0)
    if(s[i] == c)
      res++;
  return res;
}