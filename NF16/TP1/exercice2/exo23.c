#include <stdio.h>

int main(int argc, char const *argv[])
{
  char c;
  int quit = 0;
  int table;

  while(quit != 1){

    printf("Quelle table de multiplication voulez-vous, tapez 0(zéro) pour sortir ?");
    scanf("%c", &c);

    if(c > 48 && c < 58){
      table = ((int)c) - 48;
      for(int i = 0; i < 11; i++){
        printf("%d x %d = %d\n", table, i, table*i);
      }
    } else if(c == 48) {
      quit = 1;
      printf("Fermeture du programme !\n");
    } else if(c < 48 | c > 57){
      printf("Ce n'est pas dans les possibilités du programme, recommencez !\n");
    }
  }

  return 0;
}
