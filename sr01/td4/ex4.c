#include <stdio.h>
#include <math.h>

int jeu_ordi(int nb_allum, int prise_max);
void afficher_allumettes(int n);

int main ()
{
  int nb_max_d=0; /*nbre d'allumettes maxi au départ*/
  int nb_allu_max=0; /*nbre d'allumettes maxi que l?on peut tirer au maxi*/
  int qui = 1; /*qui joue? 0=Nous --- 1=PC*/
  int prise = 0; /*nbre d'allumettes prises par le joueur*/
  int nb_allu_rest=0; /*nbre d'allumettes restantes*/
  printf("Entrez le nombre d'allumette de départ : ");
  scanf("%d", &nb_max_d);
  printf("Entrez le nombre d'allumette maximum retirable par tour : ");
  scanf("%d", &nb_allu_max);
  nb_allu_rest = nb_max_d;
  while(nb_allu_rest > 0) {
    afficher_allumettes(nb_allu_rest);
    printf("\n \n");
    if(qui==1)
      qui = 0;
    else
      qui = 1;
    if(qui==1) {
      printf("-- Tour de Ordi -- \n");
      prise = jeu_ordi(nb_allu_rest, nb_allu_max);
      nb_allu_rest = nb_allu_rest - prise;
      printf("Ordi a retiré %d allumettes. \n Il reste %d allumettes. \n", prise, nb_allu_rest);
    }
    else {
      printf("-- Tour de Joueur -- \n");
      do {
        printf("Entrez le nombre d'allumette à retirer (maximum %d) : ", nb_allu_max);
        scanf("%d", &prise);
      } while(prise<1 || prise>nb_allu_max || prise>nb_allu_rest);
      nb_allu_rest = nb_allu_rest - prise;
      printf("Joueur a retiré %d allumettes. \n Il reste %d allumettes. \n", prise, nb_allu_rest);
    }
  }
  if(qui==0)
    printf("Ordi a gagné la partie. \n");
  else
    printf("Joueur a gagné la partie. \n");
}


  int jeu_ordi(int nb_allum, int prise_max) {
    int prise = 0;
    int s = 0;
    float t = 0;
    s = prise_max + 1;
    t = ((float) (nb_allum - s)) / (prise_max + 1);
    while (t != floor (t))
      {
       s--;
       t = ((float) (nb_allum-s)) / (prise_max + 1);
      }
    prise = s - 1;
    if (prise == 0)
    prise = 1;
    return (prise);
  }

  void afficher_allumettes(int n) {
    for(int i=0;i<n;i++)
      printf(" o ");
    printf("\n");
    for(int i=0;i<n;i++)
      printf(" | ");
  }
