#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DIM 10


void initialiser_matrice(int mat[DIM][DIM]);
int nb_voisins(int mat[DIM][DIM], int c, int l);
void afficher_matrice(int mat[DIM][DIM]);
void next_generation(int mat[DIM][DIM]);


/* ---------------------------------------- */


int main(int argc, char const *argv[]){

  srand(time(NULL));

  int mat[DIM][DIM];

  initialiser_matrice(mat);

  for(int i = 0; i < 5; i++){
    printf("\n--- Génération %d ---\n\n", i);
    afficher_matrice(mat);
    next_generation(mat);
  }

  printf("\n");

  return 0;
}

/* ---------------------------------------- */

void initialiser_matrice(int mat[DIM][DIM]){
  for(int i = 0; i < DIM; i++){
    for(int j = 0; j < DIM; j++){
      mat[i][j] = rand() % 2;
    }
  }
}

/* ---------------------------------------- */

int nb_voisins(int mat[DIM][DIM], int c, int l){
  int k = 0;

  for(int i = l-1; i <= l + 1; i++){
    for(int j = c-1; j <= c+1; j++){

      if(i < 0 && j < 0){
        if(mat[DIM-1][DIM-1] == 1) k++;
      }
      else if(i > DIM-1 && j > DIM-1){
        if(mat[0][0] == 1) k++;
      }
      else if(i < 0){
        if(mat[DIM-1][j] == 1) k++;
      }
      else if(j < 0){
        if(mat[i][DIM-1] == 1) k++;
      }
      else if(i > DIM-1){
        if(mat[0][j] == 1) k++;
      }
      else if(j > DIM-1){
        if(mat[i][0] == 1) k++;
      }
      else if(i != l || j != c){
        if(mat[i][j] == 1) k++;
      }
    }

  }

  return k;
}

/* ---------------------------------------- */

void afficher_matrice(int mat[][DIM]){
  for(int i = 0; i < DIM; i++){
    for(int j = 0; j < DIM; j++){
      printf("%d ", mat[i][j]);
    }
    printf("\n");
  }
}
/* ---------------------------------------- */


void next_generation(int mat[DIM][DIM]){

  int nb;

  int temp[DIM][DIM];
  for(int i = 0; i < DIM; i++){
    for(int j = 0; j < DIM; j++){
      temp[i][j] = 0;
    }
  }

  for(int i = 0; i < DIM; i++){
    for(int j = 0; j < DIM; j++){
      
      nb = nb_voisins(mat, i, j);

      if(mat[i][j] == 1){
        if(nb == 2 || nb == 3){
          temp[i][j] = 1;
        }
        if(nb >= 4){
          temp[i][j] = 0;
        }
        if(nb <= 1){
          temp[i][j] = 0;
        }
      }else if(nb == 3){
        temp[i][j] = 1;
      }else{
        temp[i][j] = 0;
      }
    } 
  }

  for(int i = 0; i < DIM; i++){
    for(int j = 0; j < DIM; j++){
      mat[i][j] = temp[i][j];
    }
  }

}