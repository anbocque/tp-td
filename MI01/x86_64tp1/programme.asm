; programme.asm
;
; MI01 - TP Assembleur 1
;
; Affiche un caract�re � l'�cran

title       programme.asm

; D�claration des fonctions de biblioth�que 'C' utilis�es dans le programme
extern      putchar:near
extern      getchar:near

.data

cara        db  'A'

.code

; Sous-programme main, automatiquement appel� par le code de
; d�marrage 'C'
public      main
main        proc
            sub     rsp, 32     ; N�cessaire pour les appels
                                ; de fonction (shadow space)

            ; Conversion du caract�re en un double mot
            movzx   ecx, byte ptr[cara]

            ; Appel � la fonction de biblioth�que 'C' int putchar(int c) 
            ; pour afficher un caract�re. La taille du type C 'int' 
            ; est de 32 bits. Le caract�re doit �tre fourni
            ; dans le registre ecx.
            ; Attention : rax, rcx, rdx, r8, r9, r10, r11 peuvent �tre modifi�s
            ; par putchar !
            call    putchar     ; Appel de putchar

            ; Appel � la fonction de biblioth�que 'C' int getchar(void)
            call    getchar     ; Attente de l'appui sur "Entr�e"

            add     rsp, 32     ; Lib�ration du shadow space
            ret                 ; Retour au code de d�marrage 'C'

main        endp

            end