#include <stdio.h>
#include <stdlib.h>

struct Structmonome{
  int coef;
  int deg;
  struct Structmonome* suivant;
};

typedef struct Structmonome* Polynome;

Polynome polynomeconstructeur(int c, int d, Polynome P);
int degre(Polynome p);
void afficherPolynome(Polynome p);

int main(int argc, char const *argv[])
{


  return 0;
}

Polynome polynomeconstructeur(int c, int d, Polynome p){
  struct Structmonome *Monome = malloc(sizeof(struct Structmonome));
  if(Monome == NULL){
    exit(EXIT_FAILURE);
  }
  Monome->coef = c;
  Monome->deg = d;
  Monome->suivant = p;

  p = Monome;

  return p;
};


int degre(Polynome p){

  int deg = -1;
  Polynome pm = p;

  while(pm != NULL){
    if(pm->deg > deg) deg = pm->deg;
    pm = pm->suivant;
  }

  return deg;
}

void afficherPolynome(Polynome p){
  Polynome pm = p;

  while(pm != NULL){
    if(pm->coef != 0){
      if(pm->deg == 0){
        printf("%d", pm->coef);
      }
      else if(pm->deg == 1) {
        printf("%d x", pm->coef);
      }
      else{
        printf("%d x^%d", pm->coef, pm->deg);
      }
    }

  if(pm->suivant->coef > 0) printf("+");

  pm = pm->suivant;
  }
}