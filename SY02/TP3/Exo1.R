runifa <- function(n){
if(!exists("param")) param <<- sample(10:20,1)
runif(n, min = 0, max = param)
}

estim <- function(X){
  moy <- sum(X) / length(X)
  return(2*moy)
}

a <- replicate(1000, estim(runifa(100)))
boxplot(a)

estim2 <- function(X, k){
mk <- sum(X^k) / length(X)
ak <- mk*(k+1)
return(ak^(1/k))
}
