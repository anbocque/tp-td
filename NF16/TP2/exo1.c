#include <stdio.h>

int fact1(int val);
int fact2(int val);

int main(int argc, char const *argv[])
{

  printf("%d\n", fact1(4));
  printf("%d\n", fact2(4));

  return 0;
}


/* --------------------------------- */

int fact1(int val){
  
  int ret = 1;

  for(int i = 1; i <= val; i++){
    ret *= i;
  }

  return ret;
}

/* --------------------------------- */

int fact2(int val){
  if(val != 1){
    return val * fact2(val - 1);
  }
}

/* --------------------------------- */

