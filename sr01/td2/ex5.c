#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define NBRE_ETUDIANTS 2

typedef struct Etudiant {
  char nom[10];
  char prenom[10];
  char compte[10];
}Etudiant;

Etudiant saisir_etudiant();
void afficher_etudiant(Etudiant);

int main(int argc, char const *argv[])
{
  Etudiant et[NBRE_ETUDIANTS];

  for(int i = 0; i<NBRE_ETUDIANTS; i++){
  et[i] = saisir_etudiant();
  }

  for(int i = 0; i<NBRE_ETUDIANTS; i++){
  afficher_etudiant(et[i]);
  }
  return 0;
}



Etudiant saisir_etudiant(){
  Etudiant et;


  printf("\nNom : ");
  scanf("%s", et.nom);
  printf("\nPrenom : ");
  scanf("%s", et.prenom);
  printf("\nCompte : ");
  scanf("%s", et.compte);

  return et;
}

void afficher_etudiant(Etudiant et){
  printf("Nom : %s\nPrénom : %s\nCompte : %s\n", et.nom, et.prenom, et.compte);
}