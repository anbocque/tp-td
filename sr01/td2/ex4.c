#include<stdio.h>
#include<stdlib.h>

struct Complexe{
	float x;
	float y;
	};

union valeur{
	int entier;
	float reel;
	struct Complexe complexe;
	};

enum genre {entier, reel, complexe}; 

typedef struct Nombre{
	enum genre  type;
	union valeur valeur;
	}Nombre ;

void afficher(Nombre);
Nombre saisir_nombre();
void afficher_nombre(Nombre);

int main(int argc, char const *argv[])
{
  Nombre nb;

  nb = saisir_nombre();

  afficher_nombre(nb);
  
  return 0;
}


void afficher(Nombre nb){
  if((int)nb.valeur.complexe.y < 0){
    printf("%f - %fi\n", nb.valeur.complexe.x, nb.valeur.complexe.y);
  }
  if((int)nb.valeur.complexe.y == 0) {
    printf("%f\n", nb.valeur.complexe.x);
  }
  if((int)nb.valeur.complexe.y > 0) {
    printf("%f + %fi\n", nb.valeur.complexe.x, nb.valeur.complexe.y);
  }
}

Nombre saisir_nombre(){
  int choix;
  Nombre nb;
  printf("1 : Entier\n 2 : Reel\n3 : Complexe\n Entrez votre choix : ");
  scanf("%d", &choix);
  switch(choix){
    case 1 : nb.type = entier;
            printf("\nEntrez la valeur : ");
            scanf("%d", &nb.valeur.entier);
            break;
    case 2 :  nb.type = reel;
            printf("\nEntrez la valeur : ");
            scanf("%f", &nb.valeur.reel);
            break;
    case 3 : nb.type = complexe;
            printf("\nEntrez la partie réelle : ");
            scanf("%f", &nb.valeur.complexe.x);
            printf("\nEntrez la partie imaginaire : ");
            scanf("%f", &nb.valeur.complexe.y);
            break;
  }
  return nb;
}


void afficher_nombre(Nombre nb) {
  switch(nb.type){
    case entier : printf("Le nombre est : %d\n", nb.valeur.entier);
                  break;
    case reel : printf("Le nombre est : %f\n", nb.valeur.reel);
                break;
    case complexe : printf("Le nombre est : ");
                    afficher(nb);
                    printf("\n");
                    break;
  }
}