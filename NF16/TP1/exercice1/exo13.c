#include <stdio.h>

int main(int argc, char const *argv[])
{
  float x, y = 0;

  printf("Entrez un nombre : ");
  scanf("%f", &y);

  printf("\nEntrez un pourcentage : ");
  scanf("%f", &x);

  printf("\n%f %% de %f = %f\n", x, y, (x*y)/100);

  return 0;
}
