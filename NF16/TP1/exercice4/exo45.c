#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
  int *tab[3];
  int k = 12;

  for(int i = 0; i < 3; i++){
    tab[i] = malloc(sizeof(int)*4);
    for(int j = 0; j < 4; j++){
      tab[i][j] = k;
      k++;
    }
  }


  for(int j = 0; j < 3; j++){
    for(int i = 0; i < 4; i++){
      printf("%d", tab[j][i]);
    }
    printf("\n");
  }


  return 0;
}
