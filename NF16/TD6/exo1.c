#include <stdio.h>
#include <stdlib.h>

#define MAX_SIZE 20

typedef struct pile {
  int tete;
  int pile[MAX_SIZE];
} pile_t;

pile_t* creer_pile();
int pile_vide(pile_t* pile);
void empiler(pile_t* pile, int val);
int depiler(pile_t* pile);

int main(int argc, char const *argv[])
{
  pile_t* pile = creer_pile();

  printf("%d\n", pile->tete);
  printf("%d\n", pile_vide(pile));

  empiler(pile, 7);
  empiler(pile, 2);

  printf("%d\n", depiler(pile));
  printf("%d\n", pile_vide(pile));


  return 0;
}

/* -------------------------------------------- */

pile_t* creer_pile(){
  pile_t* pile = malloc(sizeof(pile_t));
  pile->tete = 0;
  return pile;
};

int pile_vide(pile_t* pile){
  if(pile->tete == 0) return 1;
  return 0;
};

void empiler(pile_t* pile, int val){
  pile->pile[pile->tete] = val;
  pile->tete++;
};

int depiler(pile_t* pile){
  pile->tete--;
  int temp = pile->pile[pile->tete];
  pile->pile[pile->tete] = 0;
  return temp;
};