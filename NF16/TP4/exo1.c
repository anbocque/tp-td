#include "exo1.h"

int main(int argc, char const *argv[])
{
  t_liste liste = generer_liste_aleatoire(10);
  afficher_liste(liste);
  afficher_liste_inverse(liste);
  return 0;
}



void afficher_liste(t_liste liste){
  t_liste ptr = liste;
  while(ptr != NULL){
    printf("Noeud->%d\n", ptr->cle);
    ptr = ptr->suivant;
  }
}


t_liste generer_liste_aleatoire(int n){
  int i;
  t_noeud* noeud = NULL;
  t_liste liste = NULL;
  srand(time(NULL));
  for(i=0; i<n; i++){
    noeud = (t_noeud*)malloc(sizeof(t_noeud));
    noeud->suivant = liste;
    noeud->cle = (int) rand()%(n*10);
    liste = noeud;
  }
  return liste;
}

void afficher_liste_inverse(t_liste liste){

  if(liste->suivant){
    t_liste temp;
    t_noeud* prec = NULL;

  
    for(temp = liste; temp->suivant != NULL; temp = temp->suivant){
      prec = temp;
    }

    printf("Cle : %d\n", temp->cle);

    prec->suivant = NULL;

    afficher_liste_inverse(liste);
  } else{
    printf("Cle : %d\n", liste->cle);
  }
}