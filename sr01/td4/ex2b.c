#include <stdio.h>

#define TAILLE 10
#define NBRE 3

int main(int argc, char const *argv[])
{
  char tab[TAILLE*2];
  char tronc[TAILLE*2];

//initialisation du tronc
  for(int i = 0; i< TAILLE*2; i++){
    tronc[i] = ' ';
  }
  tronc[TAILLE] = tronc[TAILLE-1] = tronc[TAILLE+1] = '|';
  
//initialisation des feuilles
  for(int i = 0; i < TAILLE*2; i++){
    tab[i] = ' ';
  }
  tab[TAILLE] = '*';


//dessin des feuilles
  for(int j = 1; j < TAILLE+1; j++){
    for(int k = 0; k < NBRE; k++){
      for(int i = 0; i < TAILLE*2; i++){
        printf("%c", tab[i]);
      }
    }
    printf("\n");
    tab[TAILLE-j] = '*';
    tab[TAILLE+j] = '*';
  }


//dessin du tronc
  for(int j = 0; j < 4; j++){
    for(int k = 0; k < NBRE; k++){
      for(int i = 0; i < TAILLE*2; i++){
        printf("%c", tronc[i]);
      }
    }
    printf("\n");
  }

  return 0;
}
