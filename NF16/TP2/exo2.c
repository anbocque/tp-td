#include <stdio.h>
#include <stdlib.h>

#define NMAX 10
#define PI 3.14159


struct Un_Tableau_Entier{
  int tab[NMAX];
  int ncase;
};


struct menu{
  char tab[20][60];
  int n;
};

typedef enum Contenu {vin, vinaigre, biere, huile} Contenu;

typedef struct Tonneau{
  int d;
  int D;
  int L;
  Contenu contenu;
} Tonneau;

float volume1(Tonneau t){
  return PI * t.L * (t.d/2+2/3*(t.D/2-t.d/2))*(t.d/2+2/3*(t.D/2-t.d/2));
}

float volume2(Tonneau *t){
  return PI * t->L * (t->d/2+2/3*(t->D/2-t->d/2))*(t->d/2+2/3*(t->D/2-t->d/2));
}

int main(int argc, char const *argv[])
{

  Tonneau t1;
  Tonneau *t2 = malloc(sizeof(Tonneau));

  t1.d = 3;
  t1.D = 4;
  t1.L = 5;

  t2->d = 3;
  t2->D = 4;
  t2->L = 5;

  printf("%f\n", volume1(t1));
  printf("%f\n", volume2(t2));


  return 0;
}
