#include "mystrncat.h"
#include "mystrchrn.h"
#include "mystrncpy.h"
#include "mystrinv.h"
#include "mystrupdown.h"


int main(int argc, char const *argv[])
{
  printf("Sélctionner une fonction :\n");
  printf("1- mystrupdown\n");

  int c = 0;
  scanf("%d", &c);

  if(c == 1){

    char *chaine = malloc(sizeof(char)*10);
    char *ret = malloc(sizeof(char)*10);



    printf("Entrez la chaîne de caractère : ");
    fflush(stdin);
    scanf("%s", chaine);

    mystrupdown(chaine, ret);

    printf("\n%s\n", ret);

    free(chaine);
    free(ret);
  }

  return 0;
}
