#include <stdio.h>

// Programme 1
void main (){
  int A=20 , B=5;
  int C=!--A /++! B; //error: expression is not assignable
  printf ("A=%d B=%d c=%d \n", A,B,C);
}

// Programme 2
// int main (){
//   int A=20 , B=5, C= -10, D=2;
//   printf ("%d \n", A&&B ||!0&& C ++&&! D++); // 1
//   printf ("c=%d d=%d \n", C, D); // c = -10 | d = 2
// }

// Programme 3
// int main (){
//   int p[4]={1 , -2 ,3 ,4};
//   int *q=p;
//   printf("%d",*q);
//   printf ("c=%d\n", *++q**q ++); // c = 4
//   printf ("c=%d \n" ,*q); // c = 3
// }

// Programme 4
// int main (){
//   int p [4]={1 , -2 ,3 ,4};
//   int *q=p;
//   int d=*q&*q ++|* q++;
//   printf ("d=%d\n", d); // d = -1
//   printf ("q=%d \n" ,*q); // q = 3
// }

// Programme 5
// int main (){
//   int a=-8,b =3;
//   int c= ++a&&--b ? b-- :a ++;
//   printf ("a=%d b=%d c=%d\n",a,b,c); // a = -7 | b = 1 | c = 2
// }

// Programme 6
// int main (){
//   int a=-8,b =3;
//   a > >=2^b; // error: expected expression
//   printf ("a=%d\n",a); // a=-3
// }
