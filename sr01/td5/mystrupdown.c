#include "mystrupdown.h"

void mystrupdown(char *s1, char *s2){
  for(int i = 0; i < strlen(s1); i++){
    if((s1[i] >= 'A') && (s1[i] <= 'Z')) {
      s2[i] = s1[i] - 'A' + 'a';
    } else if((s1[i] >= 'a') && (s1[i] <= 'z')){
      s2[i] = s1[i] - 'a' + 'A';
    } else{
      s2[i] = s1[i];
    }
  }
}