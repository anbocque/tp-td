#include <stdio.h>
#include "ex3.h"

int main(int argc, char const *argv[])
{
  complexe a, b, c;

  a.x = 3;
  a.y = 4;

  b.x = 7;
  b.y = 8;

  c = somme(a, b);
  afficher(c);

  c = produit(a, b);
  afficher(c);

  return 0;
}

void afficher(complexe c){
  if((int)c.y < 0){
    printf("%f - %fi\n", c.x, c.y);
  }
  if((int)c.y == 0) {
    printf("%f\n", c.x);
  }
  if((int)c.y > 0) {
    printf("%f + %fi\n", c.x, c.y);
  }
}

complexe somme(complexe a, complexe b){
  complexe ret;
  ret.x = a.x + b.x;
  ret.y = a.y + b.y;

  return ret;
}

complexe produit(complexe a, complexe b){
  complexe ret;
  ret.x = a.x*b.x - a.y*b.y;
  ret.y = a.y*b.x + a.x*b.y;

  return ret;
}