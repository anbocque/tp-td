#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/wait.h>

int main(int argc, char const *argv[])
{
  pid_t pid;

  if(pid = fork()){
    wait(EXIT_SUCCESS);
    printf("Je suis le processus père avec le pid : %d\n", getpid());
  }else{
    printf("\nJe suis le processus fils avec le pid : %d\nLe pid de mon père est : %d\n", getpid(), getppid());
    exit(EXIT_SUCCESS);
  }

  return 0;
}
