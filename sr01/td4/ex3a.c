#include <stdio.h>
#include <stdlib.h>

#define N 3
#define M 4

/* -------------------------------------- */


void matrice_info(int matrice[N][M]){

  int pos = 0;
  int neg = 0;
  int zer = 0;
  
  for(int i = 0; i < N; i++){
    for(int j = 0; j < M; j++){
      if(matrice[i][j] > 0) pos++;
      if(matrice[i][j] < 0) neg++;
      if(matrice[i][j] == 0) zer++;
    }
  }

  printf("positifs : %d\n", pos);
  printf("negatif : %d\n", neg);
  printf("zeros : %d\n", zer);

  if((float)zer >= 0.8*N*M){
    printf("Matrice creuse\n");
  }
  
};

/* -------------------------------------- */


int matrice_creuse(int matrice[N][M], int *vect){

  int count = 0;

  for(int i = 0; i < N; i++){
    for(int j = 0; j < M; j++){
      if(matrice[i][j] != 0) count++;
    }
  }

  vect = malloc(sizeof(int)*count);

  count = 0;

  for(int i = 0; i < N; i++){
    for(int j = 0; j < M; j++){
      if(matrice[i][j] != 0){
        vect[count] = matrice[i][j];
        count++;
      }
    }
  }

  if((float)count+1 < 0.2*N*M) return 1;

  return 0;

}


/* -------------------------------------- */


int main(int argc, char const *argv[])
{

  int matrice[N][M] = {{1,2,3,0},
                      {4,5,0,6},
                      {7,0,8,9}};
  

  int *vect;

  matrice_info(matrice);

  printf("1 creuse, 0 pleine : %d\n", matrice_creuse(matrice, vect));
  for(int i= 0; i < 9; i++){
    printf("%d\n", vect[i]);
  }


  return 0;
}
